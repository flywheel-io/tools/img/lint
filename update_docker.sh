#!/usr/bin/env bash

replace HADOLINT=.* HADOLINT="$(latest_version git hadolint/hadolint)"
replace SHELLCHECK=.* SHELLCHECK="$(latest_version git koalaman/shellcheck)"
