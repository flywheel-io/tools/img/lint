#!/usr/bin/env bash
test -z "$TRACE" || set -x
set -eu
docker run -tv "$(pwd):/src" --rm flywheel/lint
