FROM flywheel/python-alpine:master.1a4eee7c
SHELL ["/bin/bash", "-euxo", "pipefail", "-c"]

ENV HADOLINT=2.5.0
RUN curl -fLSsO https://github.com/hadolint/hadolint/releases/download/v$HADOLINT/hadolint-Linux-x86_64; \
    chmod +x hadolint-Linux-x86_64; \
    mv hadolint-Linux-x86_64 /usr/local/bin/hadolint

ENV SHELLCHECK=0.7.2
RUN curl -fLSs https://github.com/koalaman/shellcheck/releases/download/v$SHELLCHECK/shellcheck-v$SHELLCHECK.linux.x86_64.tar.xz \
        | tar -xJ shellcheck-v$SHELLCHECK/shellcheck; \
    mv shellcheck-v$SHELLCHECK/shellcheck /usr/local/bin; \
    rm -rf shellcheck-v$SHELLCHECK

RUN apk add --no-cache nodejs-npm=14.16.1-r1; \
    npm install --global \
        jsonlint-newline-fork@1.6.8 \
        markdownlint-cli@0.27.1 \
    ; \
    rm -rf ~/.config ~/.npm

RUN apk add --no-cache --virtual .build build-base=0.5-r2; \
    pip install --no-cache-dir \
        black==21.6b0 \
        hadolintw==1.2.1 \
        pre-commit==2.13.0 \
        pydocstyle==6.1.1 \
        pyyaml==5.4.1 \
        safety==1.10.3 \
        yamllint==1.26.1 \
    ; \
    apk del .build

COPY lint /lint
ENTRYPOINT []
CMD ["/lint/run.sh"]
