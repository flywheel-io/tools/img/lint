# flywheel/lint

**NOTE:** This image is deprecated and pending removal.
Please use [tools/img/qa-ci](https://gitlab.com/flywheel-io/tools/img/qa-ci)
as a drop-in replacement.

Dockerized linting, style checks and formatting for both development and CI.

## Usage

This image is used within the `test:flywheel-lint` pre-commit hook and CI job in
[`tools/etc/qa-ci`](https://gitlab.com/flywheel-io/tools/etc/qa-ci).

To run the image directly on any project folder:

```bash
docker run --rm -tv $(pwd):/src flywheel/lint
```

## Included tools

- [`black`](https://github.com/psf/black)
- [`hadolint`](https://github.com/hadolint/hadolint)
- [`jsonlint`](https://www.npmjs.com/package/jsonlint)
- [`markdownlint`](https://github.com/DavidAnson/markdownlint)
- [`pydocstyle`](https://github.com/PyCQA/pydocstyle)
- [`safety`](https://github.com/pyupio/safety)
- [`shellcheck`](https://github.com/koalaman/shellcheck)
- [`yamllint`](https://github.com/adrienverge/yamllint)

## Configuration

Arguments passed to the linters can be extended (or overridden) using environment
variables, which are read from a `.env` file at the project root if present. For
example, to ignore an error in `shellcheck`, add the following to your `.env`:

```bash
# add an extra exclude, leaving any default args intact
SHELLCHECK_EXTRA="--exclude=SC2061"
```

The default arguments passed to the tools are recommended to be kept as-is, but
can also be overridden by using the `_ARGS` envvars (instead of `_EXTRA`):

```bash
# override the default google convention (see defaults below)
PYDOCSTYLE_ARGS="--convention=pep257"
```

Finally, the individual tools may support loading further custom settings from a
default config location if it exists at the project root:

<!-- markdownlint-disable MD013 -->
| Tool           | Envvar in `.env`            | Default `ARGS`                      | Config file |
| :------------- | :-------------------------- | :---------------------------------- | :---------- |
| `black`        | `BLACK_EXTRA`/`ARGS`        | none                                | [`pyproject.toml`](https://github.com/psf/black#configuration-format)|
| `hadolint`     | `HADOLINT_EXTRA`/`ARGS`     | `--ignore DL3005 --ignore DL3059`   | [`.hadolint.yaml`](https://github.com/hadolint/hadolint#configure)|
| `jsonlint`     | `JSONLINT_EXTRA`/`ARGS`     | `--in-place --insert-final-newline` | none |
| `markdownlint` | `MARKDOWNLINT_EXTRA`/`ARGS` | `--fix`                             | [`.markdownlint.json`](https://github.com/DavidAnson/markdownlint#optionsconfig)|
| `pydocstyle`   | `PYDOCSTYLE_EXTRA`/`ARGS`   | `--convention=google`               | [`.pydocstyle.ini`](http://www.pydocstyle.org/en/stable/snippets/config.html)|
| `shellcheck`   | `SHELLCHECK_EXTRA`/`ARGS`   | `--external-sources --color=always` | [`.shellcheckrc`](https://github.com/koalaman/shellcheck/blob/master/shellcheck.1.md#rc-files)|
| `yamllint`     | `YAMLLINT_EXTRA`/`ARGS`     | `-f colored`                        | [`.yamllint.yml`](https://yamllint.readthedocs.io/en/stable/configuration.html#extending-the-default-configuration)|
<!-- markdownlint-enable -->

If not present, the following config files are auto-injected:

- [`.markdownlint.json`](lint/.markdownlint.json)
- [`.yamllint.yml`](lint/.yamllint.yml)

## Development

- Run `pre-commit install`
- Add new tools in the [`Dockerfile`](Dockerfile)
- Add new hooks to [`lint/hooks.yml`](lint/hooks.yml)

## Publishing

Images are published to [dockerhub](https://hub.docker.com/repository/docker/flywheel/lint/tags?page=1&ordering=last_updated)
on every successful CI build.

## License

[![MIT](https://img.shields.io/badge/license-MIT-green)](LICENSE)
